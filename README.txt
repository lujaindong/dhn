为了仿真philly:
运行的命令：
D:\jdlu\Anaconda3\python.exe D:/DHN_code/philly_2_news_final/main.py --input-training-data-path D:\DHN_git\data --output-model-path D:\DHN_git\data

字段说明：
T2U_feat：user所独有的连续型特征（填充缺失值）,对于每个样本都是定长的连续型特征向量 <需要设置的参数：这个特征向量的长度>
T2I_feat：item所独有的连续型特征（填充缺失值）,对于每个样本都是定长的连续型特征向量 <需要设置的参数：这个特征向量的长度>

T2U_pair_feat：用户特有的配对的连续型特征<field的个数>(几个field，每个field的特征向量的维度，比如, dssm->128维， 如果缺失，则补上0,维度保持一致)
T2I_pair_feat：用户特有的配对的连续型特征<field的个数>(几个field，每个field的特征向量的维度，比如, dssm->128维, 如果缺失，则补上0,维度保持一致)

###
T1U_pair_feat:user端可以配对的ffm类型的特征，如果对于某个field是缺失的，那么就补成field:0:0 (field index start by 1)（T1U_pair_feat里面增加一个field，作为user曾经浏览过的特征）
T1I_pair_feat:item端可以配对的ffm类型的特征，如果对于某个field是缺失的，那么就补成field:0:0 (field index start by 1)（newItemid放在T1I_pair_feat里面，作为item field里面的一个id）
这块sparse feat使用一个embedding矩阵(field的个数，以及该sparse矩阵的feat index的最大值）

T0U_unique_feat:user端独有的ffm类型的特征，每个样本如果某个field缺失，那么补成field:0:0 (field index start by 1)(newUid放在T0U_unique_feat,因为只有user端有该特征)
这块sparse feat使用一个embedding矩阵（需要的参数：field的个数，feat的个数）

T0I_unique_feat:item端独有的ffm类型的特征，每个样本如果某个field缺失，那么补成field:0:0 (field index start by 1)<field的个数>
这块sparse feat使用一个embedding矩阵（需要的参数：field的个数，feat的个数）

参数说明：
'T2U_feat_len': 5,  ==>T2U_feat的维度
'T2I_feat_len': 2,  ==>T2I_feat的维度
'T2_pair_feat_num': 10, ==>T2U_pair_feat的维度和T2I_pair_feat的维度，2个维度是一致的
'T1_pair_field_num': 3, ==>T1U_pair_feat域的个数
'T1_pair_feat_num': 101, ==>T1U_pair_feat特征的个数
'T0U_unique_field_num': 4, ==>T0U_unique_feat域的个数
'T0U_unique_feat_num': 201, ==>T0U_unique_feat特征的个数
'T0I_unique_field_num': 5, ==>T0I_unique_feat域的个数
'T0I_unique_feat_num': 301, ==>T0I_unique_feat特征的个数