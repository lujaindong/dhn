import sys
import random

#需要设置的参数
T2U_feat_len = 5
T2I_feat_len = 2
T2_pair_feat_num = 10
T1_pair_field_num = 3
T1_pair_feat_num = 100
T0U_unique_field_num = 4
T0U_unique_feat_num = 200
T0I_unique_field_num = 5
T0I_unique_feat_num = 300
NUM = 200

out = open('./eval_dhn_final.csv', 'w')
for i in range(NUM):
    feats = []
    p = random.random()
    if p < 0.8:
        label = '0'
    else:
        label = '1'
    feats.append(label)

    T2U_feat = 'T2U_feat#'+','.join([str(val) for val in [random.random() for i in range(T2U_feat_len)]])
    feats.append(T2U_feat)

    T2I_feat = 'T2I_feat#'+','.join([str(val) for val in [random.random() for i in range(T2I_feat_len)]])
    feats.append(T2I_feat)

    T2U_pair_feat = 'T2U_pair_feat#dssmU:' + ','.join([str(val) for val in [random.random() for i in range(T2_pair_feat_num)]])
    feats.append(T2U_pair_feat)

    T2I_pair_feat = 'T2I_pair_feat#dssmI:' + ','.join([str(val) for val in [random.random() for i in range(T2_pair_feat_num)]])
    feats.append(T2I_pair_feat)

    T1U_pair_feat_list = []
    T1U_field_num = random.randint(1, T1_pair_field_num)
    for field in range(T1U_field_num):
        feat_index = random.randint(1, T1_pair_feat_num)
        T1U_pair_feat_list.append(str(field+1)+':'+str(feat_index)+':1')
    T1U_pair_feat = 'T1U_pair_feat#'+','.join(T1U_pair_feat_list)
    feats.append(T1U_pair_feat)

    T1I_pair_feat_list = []
    T1I_field_num = random.randint(1, T1_pair_field_num)
    for field in range(T1I_field_num):
        feat_index = random.randint(1, T1_pair_feat_num)
        T1I_pair_feat_list.append(str(field+1)+':'+str(feat_index)+':1')
    T1I_pair_feat = 'T1I_pair_feat#'+','.join(T1I_pair_feat_list)
    feats.append(T1I_pair_feat)

    T0U_unique_feat_list = []
    T0U_field_num = random.randint(1, T0U_unique_field_num)
    for field in range(T0U_field_num):
        feat_index = random.randint(1, T0U_unique_feat_num)
        T0U_unique_feat_list.append(str(field+1)+':'+str(feat_index)+':1')
    T0U_unique_feat = 'T0U_unique_feat#'+','.join(T0U_unique_feat_list)
    feats.append(T0U_unique_feat)

    T0I_unique_feat_list = []
    T0I_field_num = random.randint(1, T0I_unique_field_num)
    for field in range(T0I_field_num):
        feat_index = random.randint(1, T0I_unique_feat_num)
        T0I_unique_feat_list.append(str(field+1)+':'+str(feat_index)+':1')
    T0I_unique_feat = 'T0I_unique_feat#'+','.join(T0I_unique_feat_list)
    feats.append(T0I_unique_feat)
    line = '|'.join(feats) + '\n'
    out.write(line)
out.close()
