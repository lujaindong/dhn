"""This script parse and run train function"""
import train
import util as util
import tensorflow as tf
import sys
import os
import platform
import socket
import logging
import argparse


# system
def get_os_name():
    return platform.system()


def get_host_name():
    return platform.node()


def get_host_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


def get_python_version_info():
    return sys.version_info


def get_environment_variable(name, default=None):
    if name not in os.environ:
        return default
    return os.environ[name]


logging.basicConfig(stream=sys.stdout, format='[%(asctime)s] %(levelname)s: %(message)s', level=logging.INFO)

logging.info("begin")
logging.info("===============================")
logging.info("os=%s", get_os_name())
logging.info("host=%s", get_host_name())
logging.info("ip=%s", get_host_ip())
logging.info("python=%s", get_python_version_info()[0:3])

logging.info("===============================")
logging.info("tf version=%s", tf.__version__)
logging.info("CUDA_VISIBLE_DEVICES=%s", get_environment_variable("CUDA_VISIBLE_DEVICES"))
logging.info("os.environ=%s", os.environ)

logging.info("===============================")
logging.info("sys.argv=%s", sys.argv)

logging.info("begin")
logging.info("===============================")
logging.info("os=%s", get_os_name())
logging.info("host=%s", get_host_name())
logging.info("ip=%s", get_host_ip())
logging.info("python=%s", get_python_version_info()[0:3])

logging.info("===============================")
logging.info("tf version=%s", tf.__version__)
logging.info("CUDA_VISIBLE_DEVICES=%s", get_environment_variable("CUDA_VISIBLE_DEVICES"))
logging.info("os.environ=%s", os.environ)

logging.info("===============================")
logging.info("sys.argv=%s", sys.argv)


def GetParsedArguments(pattern):
    parsed_results = {}
    parser = argparse.ArgumentParser()
    for arg in sys.argv:
        if arg.startswith(pattern):
            parser.add_argument(arg, nargs='?')

    (args, unknown) = parser.parse_known_args()

    for arg in sorted(vars(args)):
        value = getattr(args, arg)
        parsed_results[arg] = value
    return parsed_results


dic_input_dir = GetParsedArguments("--input-")
dic_output_dir = GetParsedArguments("--output-")

if dic_input_dir is None or dic_output_dir is None:
    logging.info("can not parse input/ouput arguments")
    sys.exit(0)

logging.info(dic_input_dir)
logging.info(dic_output_dir)


def create_hparams(FLAGS):
    """Create hparams."""
    return tf.contrib.training.HParams(
        # data
        train_file=FLAGS['train_file'] if 'train_file' in FLAGS else None,
        eval_file=FLAGS['eval_file'] if 'eval_file' in FLAGS else None,
        T2U_feat_len=FLAGS['T2U_feat_len'] if 'T2U_feat_len' in FLAGS else None,
        T2I_feat_len=FLAGS['T2I_feat_len'] if 'T2I_feat_len' in FLAGS else None,
        T2_pair_feat_num=FLAGS['T2_pair_feat_num'] if 'T2_pair_feat_num' in FLAGS else None,
        T1_pair_field_num=FLAGS['T1_pair_field_num'] if 'T1_pair_field_num' in FLAGS else None,
        T1_pair_feat_num=FLAGS['T1_pair_feat_num'] if 'T1_pair_feat_num' in FLAGS else None,
        T0U_unique_field_num=FLAGS['T0U_unique_field_num'] if 'T0U_unique_field_num' in FLAGS else None,
        T0U_unique_feat_num=FLAGS['T0U_unique_feat_num'] if 'T0U_unique_feat_num' in FLAGS else None,
        T0I_unique_field_num=FLAGS['T0I_unique_field_num'] if 'T0I_unique_field_num' in FLAGS else None,
        T0I_unique_feat_num=FLAGS['T0I_unique_feat_num'] if 'T0I_unique_feat_num' in FLAGS else None,

        # model
        model_type=FLAGS['model_type'] if 'model_type' in FLAGS else None,
        layer_sizes=FLAGS['layer_sizes'] if 'layer_sizes' in FLAGS else None,
        activation=FLAGS['activation'] if 'activation' in FLAGS else None,
        load_model_name=FLAGS['load_model_name'] if 'load_model_name' in FLAGS else None,
        dim=FLAGS['dim'] if 'dim' in FLAGS else None,

        # train
        init_method=FLAGS['init_method'] if 'init_method' in FLAGS else 'tnormal',
        init_value=FLAGS['init_value'] if 'init_value' in FLAGS else 0.01,
        embed_l2=FLAGS['embed_l2'] if 'embed_l2' in FLAGS else 0.0000,
        embed_l1=FLAGS['embed_l1'] if 'embed_l1' in FLAGS else 0.0000,
        layer_l2=FLAGS['layer_l2'] if 'layer_l2' in FLAGS else 0.0000,
        layer_l1=FLAGS['layer_l1'] if 'layer_l1' in FLAGS else 0.0000,
        learning_rate=FLAGS['learning_rate'] if 'learning_rate' in FLAGS else 0.001,
        loss=FLAGS['loss'] if 'loss' in FLAGS else None,
        optimizer=FLAGS['optimizer'] if 'optimizer' in FLAGS else 'adam',
        epochs=FLAGS['epochs'] if 'epochs' in FLAGS else 10,
        batch_size=FLAGS['batch_size'] if 'batch_size' in FLAGS else 1,

        # show info
        show_step=FLAGS['show_step'] if 'show_step' in FLAGS else 1,
        metrics=FLAGS['metrics'] if 'metrics' in FLAGS else None,
        save_epoch=FLAGS['save_epoch'] if 'save_epoch' in FLAGS else None,

        # save model
        model_dir=FLAGS['model_dir'] if 'model_dir' in FLAGS else None,
        cache_path=FLAGS['cache_path'] if 'cache_path' in FLAGS else None,
    )


def main():
    """main function"""
    # flag = True
    config = {
        'T2U_feat_len': 5,
        'T2I_feat_len': 2,
        'T2_pair_feat_num': 10,
        'T1_pair_field_num': 3,
        'T1_pair_feat_num': 101,
        'T0U_unique_field_num': 4,
        'T0U_unique_feat_num': 201,
        'T0I_unique_field_num': 5,
        'T0I_unique_feat_num': 301,
        'model_type': 'Dhn',
        'layer_sizes': [100, 100],
        'activation': ['relu', 'relu'],
        'dim': 10,
        'init_method': 'tnormal',
        'init_value': 0.001,
        'embed_l2': 0.0,
        'embed_l1': 0.0,
        'layer_l2': 0.0,
        'layer_l1': 0.0,
        'learning_rate': 0.0001,
        'loss': 'square_loss',
        'optimizer': 'adam',
        'epochs': 10,
        'batch_size': 3,
        'show_step': 10,
        'save_epoch': 2
    }
    train_file = dic_input_dir['input_training_data_path'] + '\\train_dhn_final.csv'
    eval_file = dic_input_dir['input_training_data_path'] + '\\eval_dhn_final.csv'
    config['train_file'] = train_file
    config['eval_file'] = eval_file

    cache_dir = dic_input_dir['input_training_data_path'] + '/cache/'
    dic_input_dir['cache_path'] = cache_dir
    if not os.path.isdir(dic_input_dir['cache_path']):
        os.makedirs(dic_input_dir['cache_path'], exist_ok=True)
    config['cache_path'] = cache_dir

    model_dir = dic_output_dir['output_model_path'] + '/model/'
    dic_output_dir['output_model_path'] = model_dir
    if not os.path.isdir(dic_output_dir['output_model_path']):
        os.makedirs(dic_output_dir['output_model_path'], exist_ok=True)
    config['model_dir'] = model_dir
    hparams = create_hparams(config)
    train.train(hparams)
    logging.info("end")


main()
