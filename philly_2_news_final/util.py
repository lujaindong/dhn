"""define util function and  global variable"""
import tensorflow as tf
import os, sys
import time


# CACHE_DIR = './cache/'
# MODEL_DIR = './checkpoint/'

# def check_and_mkdir():
#     def make_dir(DIR):
#         if not os.path.exists(DIR):
#             os.mkdir(DIR)
#     make_dir(CACHE_DIR)
#     make_dir(MODEL_DIR)


def check_tensorflow_version():
    if tf.__version__ < "1.2.0":
        raise EnvironmentError("Tensorflow version must >= 1.2.0,but version is {0}". \
                               format(tf.__version__))


def print_time(s, start_time):
    """Take a start time, print elapsed duration, and return a new time."""
    print("%s, %ds, %s." % (s, (time.time() - start_time), time.ctime()))
    sys.stdout.flush()
    return time.time()


def check_file_exist(filename):
    if not os.path.isfile(filename):
        raise ValueError("{0} is not exits".format(filename))

# def convert_cached_name(file_name, batch_size):
#     prefix = CACHE_DIR + 'batch_size_' + str(batch_size) + '_'
#     prefix += (file_name.strip().split('/'))[-1]
#     train_cache_name = prefix.replace(".txt", ".tfrecord"). \
#         replace(".csv", ".tfrecord"). \
#         replace(".libsvm", ".tfrecord")
#     return train_cache_name
#
#
# def convert_res_name(file_name):
#     prefix = RES_DIR
#     inferfile = file_name.split('/')[-1]
#     res_name = prefix + inferfile.replace("tfrecord", "res.csv"). \
#         replace(".csv", ".tfrecord"). \
#         replace(".libsvm", ".tfrecord")
#     return res_name
